# Automation Practice - Sauce Labs

A small automation exercise to demonstrate some e-commerce flows from saucedemo.com. A demo of the ran tests can be found [here](https://gitlab.com/ajamescode/automation-practice/-/blob/main/cypress/videos/assignment.cy.ts.mp4).

## How to execute tests

1. Create a folder and clone the repo

```sh
mkdir ajexercise
cd ajexercise
git clone https://gitlab.com/ajamescode/automation-practice
cd automation-practice
```

2. install node dependencies

```sh
yarn install
```

3. run the tests (may install cypress helper app the first time)

```sh
yarn cypress:run
```

The console should output the result of the tests, and the cypress/videos folder should contain a recorded video of the test.

## Answers to questions

### Question 1

What challenges did you face during this exercise?

#### Answer 1

1.1. Figuring out real-world vs. choosing arbitrary hypothetical requirements. As most decisions are never made in a vacuum, some things would have made some decisions easier / harder. For instance:

- Is this a test of a webpage we control or would saucelabs represent a 3rd party site we are testing?
- Does the test need to run from a mobile version of webkit?
- Can I get away with simply changing the UserAgent and viewport size?
- Who needs to reuse this framework and who are the stakeholders?

Many of these hypotheticals would influence a lot of the decision making, so trying to come up with arbitrary requirements was difficult without context to lean on.

1.2. I also had some trouble with the first website. That set me back which changed the amount of time I had to work on it. With a shortened deadline, I cut a lot of corners and did not get a chance to learn the best practices for Cypress.

### Question 2

Why did you choose the framework that you chose?

#### Answer 2

2.1. I mostly chose Cypress for lack of friction. I had familiarity with TypeScript and needed intellesense / autocomplete in order to play with API. Java would have been a good choice which would equate to using Selenium, but many times I fear comitting to a Java project. Java will have some environment surprises and I did not have a day or two to mess with my machine setup. It was a calculated risk, but Cypress appeared to be easy to setup and allowed me the type safety of TypeScript.

2.2 After reading Cypress' documentation, I thought the API was really intuitive. This isn't to take away from other APIs, but the documentation of Cypress and its assertion chains were enticing. When given a time crunch, many times the easiest framework wins. This is probably a repeat of the previous answer; lack of friction.
