describe('SauceDemo flows', () => {
  beforeEach(() => {
    cy.viewport('iphone-x');
    cy.clearCookies();
  });

  const login = (username: string, password: string) => {
    cy.get('input[id=user-name]').type(username);
    cy.get('input[id="password"]').type(password);
    cy.get('input[id="login-button"]').click();
  };

  it('Shows an error with invalid credentials', () => {
    cy.visit('http://www.saucedemo.com/');
    login('abc', 'def');
    cy.get('[data-test="error"]').should('not.be.empty');
  });

  it('Checkout happy path', () => {
    // Login and check inventory landing
    cy.visit('http://www.saucedemo.com/');
    login('standard_user', 'secret_sauce');
    cy.url().should('include', '/inventory.html');

    // Click on first add to cart button and checkout
    cy.get('button[data-test*="add-to-cart-sauce-labs-"]').first().click();
    cy.get('#shopping_cart_container a').click();
    cy.get('button[data-test="checkout"]').click();

    // Fill out the checkout information
    cy.get('input[data-test="firstName"]').type('Bob');
    cy.get('input[data-test="lastName"]').type('Smith');
    cy.get('input[data-test="postalCode"]').type('77777');
    cy.get('input[data-test="continue"]').click();

    // Wait as a visual for video recording
    cy.wait(2000);

    // Finish up and logout
    cy.get('button[data-test="finish"]').click();
    cy.get('#logout_sidebar_link').click({ force: true });
  });
});
